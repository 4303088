# modCLI

modCLI is a wrapper for the MODX Revolution Processors, allowing you to pretty much do anything from the commandline
that you would normally do within the manager. 

To use modCLI, simply download the modcli.php file and put it in the MODX_BASE_PATH of your installation. Next open
up the console or terminal, and start firing some commands at it.

## Syntax

    php modcli.php processor [field=value [field=value]..]

The _processor_ needs to be a valid processor in your core/model/modx/processors/ directory, minus the extension. 
Specifying field=value pairs will pass these on as properties to the processor, just like using AJAX in the manager would.

By specifying the "debug" keyword anywhere in the command, you will get to see a bit more of information. By specifying a
processors_path=/absolute/path/to/processors/ you can override the path to look in for processors, for example to hit up
a processor belonging to a different package.

## Examples

Create a new resource with a pagetitle and content under resource 6.

    php modcli.php resource/create pagetitle=Awesome! parent=6 content=Awesomer!
   
````
# Running modCLI 0.1.0-pl in modcli.php
# Processor: resource/create
# Properties: 
#    pagetitle       => Awesome! 
#    parent          => 6 
#    content         => Awesomer! 

> Object retrieved: 
>    id              => 20
````

Create a new resource with a pagetitle and content under resource 6, this time with debug enabled.

    php modcli.php resource/create pagetitle=Awesome! parent=6 content=Awesomer! debug

````
# Running modCLI 0.1.0-pl in modcli.php
# Debug is enabled.
# Processor: resource/create
# Properties: 
#    pagetitle       => Awesome! 
#    parent          => 6 
#    content         => Awesomer! 

> Raw response: Array
(
    [success] => 1
    [message] => 
    [total] => 0
    [errors] => Array
        (
        )

    [object] => Array
        (
            [id] => 21
        )

)

> Response type: get 
> Object retrieved: 
>    id              => 21
```

List the packages in the manager, starting at #2 and only returning one result.

    php modcli.php workspace/packages/getlist limit=1 start=2
    
````
# Running modCLI 0.1.0-pl in modcli.php
# Processor: workspace/packages/getlist
# Properties: 
#    limit           => 1 
#    start           => 2 

> Amount of Results: 8 
>   Result 1:
>      signature       => login-1.8.1-pl
>      created         => Sep 07, 2012 12:59 AM
>      updated         => Sep 07, 2012 01:01 AM
>      installed       => Sep 07, 2012 01:01 AM
>      state           => 0
>      workspace       => 1
>      provider        => 0
>      disabled        => 
>      source          => login-1.8.1-pl.transport.zip
>      manifest        => 
>      package_name    => login
>      version_major   => 1
>      version_minor   => 8
>      version_patch   => 1
>      release         => pl
>      release_index   => 0
>      provider_name   => 
>      name            => login
>      version         => 1.8.1
>      iconaction      => icon-uninstall
>      textaction      => Uninstall
>      readme          => 
>        --------------------<br />
>        Snippet: Login<br />
>        --------------------<br />
>        Version: 1.7<br />
>        Sin...
>      updateable      => 
````


## Limitations, bugs & other things
Currently, the properties only support a single word (every space denotes a next field). Multi-word properties are on the roadmap but need to be implemented..

### Using PHP on the commandline when running MAMP (Mac)
When using modCLI on Mac running MAMP, you may get weird errors about PDO::CONNECT and what not with a number of processors. 
This is because it is using the Mac-included PHP binary, and not the one from MAMP. To get past this issue, add the following
lines to your ~/.bash_profile (Of course updating the path depending on your version of both MAMP and PHP):

    alias mphp='/Applications/MAMP/bin/php/php5.4.4/bin/php'
    
and restarting your terminal or typing `. ~/.bash_profile` to load in the new configuration.

After you did that you can start using the `mphp` command instead of `php` in the above examples, which should then work. 
You can test if the binary is properly loaded with `mphp --version`. 